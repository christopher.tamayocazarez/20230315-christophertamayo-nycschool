package com.tamayo.nycschoolsjpmc

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController
import com.tamayo.nycschoolsjpmc.ui.theme.NYCSchoolsJPMCTheme
import com.tamayo.nycschoolsjpmc.view.NavGraph
import com.tamayo.nycschoolsjpmc.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NYCSchoolsJPMCTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color.White
                ) {

                    /*
                    Call my NavGraph when activity is created
                     */
                    val navController = rememberNavController()
                    val schoolViewModel: SchoolViewModel = hiltViewModel()

                    NavGraph(schoolViewModel = schoolViewModel, navHostController = navController)

                }
            }
        }
    }
}

