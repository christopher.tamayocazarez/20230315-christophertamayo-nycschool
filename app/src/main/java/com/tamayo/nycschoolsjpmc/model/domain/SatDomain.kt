package com.tamayo.nycschoolsjpmc.model.domain

import com.tamayo.nycschoolsjpmc.model.SATScores


data class SatDomain(
    val dbm: String? = null,
    val school_name: String? = null,
    val num_of_sat_test_takers: String? = null,
    val sat_critical_reading_avg_score: String? = null,
    val sat_math_avg_score: String? = null,
    val sat_writing_avg_score: String? = null


)


fun List<SATScores>?.toDomain(): List<SatDomain> =

    this?.map {
        SatDomain(

            dbm = it.dbm ?: "DBM Not available",
            school_name = it.school_name ?: "school_name Not available",
            num_of_sat_test_takers = it.num_of_sat_test_takers ?: "num_of_sat_test_takers Not available",
            sat_critical_reading_avg_score = it.sat_critical_reading_avg_score ?: "DBM Not available",
            sat_math_avg_score = it.sat_math_avg_score ?: "sat_math_avg_score Not available",
            sat_writing_avg_score = it.sat_writing_avg_score ?: "sat_writing_avg_score Not available",

            )
    } ?: emptyList()