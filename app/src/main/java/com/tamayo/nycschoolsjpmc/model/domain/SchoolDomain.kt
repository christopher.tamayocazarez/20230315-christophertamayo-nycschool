package com.tamayo.nycschoolsjpmc.model.domain

import com.tamayo.nycschoolsjpmc.model.Schools


data class SchoolDomain(

    var dbm: String? = null,
    val name: String? = null,
    val phone: String? = null,
    val location: String? = null,
    val school_email: String? = null,
    val city: String? = null,
    val overview_paragraph: String? = null,
    val website: String? = null,
    val zip: String? = null,

    )

fun List<Schools>?.toDomain(): List<SchoolDomain> =

    this?.map {
        SchoolDomain(

            dbm = it.dbn ?: "DBN not available",
            name = it.school_name ?: "Name not available",
            phone = it.phone_number ?: "Phone not available",
            location = it.location ?: "Location not available",
            school_email = it.school_email ?: "School_email not available",
            city = it.city ?: "City not available",
            overview_paragraph = it.overview_paragraph ?: "Overview not available",
            website = it.website ?: "Website not available",
            zip = it.zip ?: "Zip not available"

        )


    } ?: emptyList()
