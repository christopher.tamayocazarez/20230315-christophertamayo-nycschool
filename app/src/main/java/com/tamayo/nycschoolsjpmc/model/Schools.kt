package com.tamayo.nycschoolsjpmc.model

import com.google.gson.annotations.SerializedName

data class Schools(
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("dbn")
    val dbn: String? = null,
    @SerializedName("school_name")
    val school_name: String? = null,
    @SerializedName("phone_number")
    val phone_number: String? = null,
    @SerializedName("overview_paragraph")
    val overview_paragraph: String? = null,
    @SerializedName("website")
    val website: String? = null,
    @SerializedName("location")
    val location: String? = null,
    @SerializedName("school_email")
    val school_email: String? = null,
    @SerializedName("zip")
    val zip: String? = null

)
