package com.tamayo.nycschoolsjpmc.utils

class NullResponse(message: String = "Connection response is null") : Exception(message)
class NotConnection(message: String = "Check your internet connection") : Exception(message)

class FailureResponse(message: String?): Exception(message)