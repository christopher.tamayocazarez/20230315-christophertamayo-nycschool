package com.tamayo.nycschoolsjpmc.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF01457C)
val Teal200 = Color(0xFF03DAC5)