package com.tamayo.nycschoolsjpmc.di

import com.tamayo.nycschoolsjpmc.rest.SchoolRepository
import com.tamayo.nycschoolsjpmc.rest.SchoolRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/*
 Bind my SchoolRepository interface to be able to Inject it in my viewModel
 */
@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideSchoolRepository(schoolRepositoryImpl: SchoolRepositoryImpl): SchoolRepository

}