package com.tamayo.nycschoolsjpmc.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/*
 Here is going to run Hilt
 */
@HiltAndroidApp
class MyApp: Application() {
}