package com.tamayo.nycschoolsjpmc.di

import com.google.gson.Gson
import com.tamayo.nycschoolsjpmc.rest.ServiceAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/*
 Here i am providing all things that i am going to Inject after
 */

/*
 SingletonComponent ensure that the provides are only created once and i am going to be re-usable
 */
@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(ServiceAPI.BASE_PATH)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()

    @Provides
    fun providesOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()

    @Provides
    fun provideGson(): Gson = Gson()

    @Provides
    fun providesHttpLoginInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }


    @Provides
    fun provideServiceApi(retrofit: Retrofit): ServiceAPI =
        retrofit.create(ServiceAPI::class.java)


    /*
    Provide Dispatchers.IO to read files when i make network requests
    and avoid blocking the main thread
     */
    @Provides
    fun provideIoDispatcher(): CoroutineDispatcher = Dispatchers.IO


}