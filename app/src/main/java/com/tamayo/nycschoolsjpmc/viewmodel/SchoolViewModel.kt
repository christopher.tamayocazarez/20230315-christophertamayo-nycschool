package com.tamayo.nycschoolsjpmc.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tamayo.nycschoolsjpmc.model.domain.SatDomain
import com.tamayo.nycschoolsjpmc.model.domain.SchoolDomain
import com.tamayo.nycschoolsjpmc.rest.SchoolRepository
import com.tamayo.nycschoolsjpmc.utils.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val TAG = "SchoolViewModel"

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val schoolRepository: SchoolRepository
) : ViewModel() {

    /*
    selectedSchool -> this variable it is going to be in charge of save the School selected
    dbn -> this variable is going to be in charge of save the dbn selected and use as a parameter in my getSatByDbn(dbn)
     */
    var selectedSchool: SchoolDomain? = null
    var dbn: String = ""

    /*
    I am going to use LiveDate to observe values in real-time
    (life cycle aware)
     */
    private val _school: MutableLiveData<UIState<List<SchoolDomain>>> =
        MutableLiveData(UIState.LOADING)
    val school: LiveData<UIState<List<SchoolDomain>>> get() = _school

    private val _sat: MutableLiveData<UIState<List<SatDomain>>> =
        MutableLiveData(UIState.LOADING)
    val sat: LiveData<UIState<List<SatDomain>>> get() = _sat


    init {
        getSchools()
        getSatByDbn()
    }


    /*
    Get all Schools from the repository
    and insert the School List in the _school(MutableLiveData)
     */
    private fun getSchools() {
        viewModelScope.launch {
            schoolRepository.getSchools().collect {
                _school.value = it
                Log.d(TAG, "getSchools: $it")
            }
        }

    }

    /*
    Get the SatScore information using the DBN selected,
    this is going to be pass when i call this function
    and insert the sat information in the _sat(MutableLiveData)
     */
    fun getSatByDbn(dbn: String? = null) {
        dbn?.let {
            viewModelScope.launch {
                schoolRepository.getSatScoresByDbm(dbn).collect {
                    _sat.value = it
                    Log.d(TAG, "getSatByDbn: $it")
                }
            }
        }

    }


}