package com.tamayo.nycschoolsjpmc.view

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.tamayo.nycschoolsjpmc.viewmodel.SchoolViewModel

@Composable
fun NavGraph(schoolViewModel: SchoolViewModel, navHostController: NavHostController) {

    NavHost(navController = navHostController, startDestination = "schoolScreen") {

        composable(route = "schoolScreen") {


            //Call the screen here
            SchoolScreen(schoolViewModel = schoolViewModel, navController = navHostController)
            schoolViewModel.getSatByDbn(schoolViewModel.dbn)




        }


        composable(route = "details") {
            //Call the screen here
            DetailScreen(schoolViewModel = schoolViewModel, navHostController)


        }


    }




}


