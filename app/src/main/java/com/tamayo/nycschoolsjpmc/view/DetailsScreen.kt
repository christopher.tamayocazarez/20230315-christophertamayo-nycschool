package com.tamayo.nycschoolsjpmc.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.tamayo.nycschoolsjpmc.R
import com.tamayo.nycschoolsjpmc.model.domain.SatDomain
import com.tamayo.nycschoolsjpmc.ui.theme.Purple700
import com.tamayo.nycschoolsjpmc.utils.UIState
import com.tamayo.nycschoolsjpmc.viewmodel.SchoolViewModel


@Composable
fun DetailScreen(schoolViewModel: SchoolViewModel, navController: NavController) {
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        topBar = {
            DetailsToolbar(navController = navController)
        }, backgroundColor = Color.White,
        scaffoldState = scaffoldState,
        content = {
            it.calculateTopPadding()
            it.calculateBottomPadding()

            when (val state = schoolViewModel.sat.observeAsState(UIState.LOADING).value) {
                is UIState.LOADING -> {
                    println("Loading")
                }
                is UIState.SUCCESS -> {

                    SchoolDetails(
                        sat = state.response.firstOrNull(),
                        schoolViewModel = schoolViewModel
                    )

                }
                is UIState.ERROR -> {

                }
            }
        })

}


@Composable
fun SchoolDetails(
    sat: SatDomain? = null,
    schoolViewModel: SchoolViewModel
) {


    val school = schoolViewModel.selectedSchool


    println("Here $school")


    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 10.dp)
            .background(color = Color.White)
            .verticalScroll(rememberScrollState())
    ) {

        Text(
            text = school?.name ?: "School name not available",
            color = Color.Black,
            fontFamily = FontFamily.Serif,
            fontSize = 25.sp,
            fontWeight = FontWeight.ExtraBold,
            textAlign = TextAlign.Start,
            modifier = Modifier.padding(top = 10.dp, start = 10.dp, end = 10.dp)
        )


        val location = school?.location?.substringAfterLast("(")
        val latitude = location?.substringBefore(",")
        val longitude = school?.location?.substringAfterLast(",")?.substringBefore(")")
        val context = LocalContext.current
        val labelLocation = school?.name




        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp, start = 10.dp, end = 10.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_map),
                contentDescription = "ic_map",
                modifier = Modifier.padding(end = 2.dp)
            )

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable(onClick = {
                        goToGoogleMaps(
                            latitude,
                            longitude,
                            labelLocation ?: "School name not availabe",
                            context
                        )
                    })
            ) {
                Text(
                    text = school?.location?.substringBefore("(") ?: "Location not available",
                    color = Color.Blue,
                    fontFamily = FontFamily.Serif,
                    fontSize = 15.sp,
                    textDecoration = TextDecoration.Underline,
                    fontWeight = FontWeight.ExtraBold,
                    textAlign = TextAlign.Start,
                )
            }
        }


        Text(
            text = "Contact Info",
            color = Color.DarkGray,
            fontSize = 20.sp,
            fontWeight = FontWeight.ExtraBold,
            textAlign = TextAlign.Start,
            modifier = Modifier.padding(top = 20.dp, start = 10.dp, end = 10.dp)
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp, start = 10.dp, end = 10.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_email),
                contentDescription = "ic_email",
                modifier = Modifier.padding(end = 2.dp)
            )

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable(onClick = {
                        school?.school_email?.let { goToEmail(it, context) }
                    })
            ) {
                Text(
                    text = school?.school_email ?: "School_email not available",
                    color = Color.Blue,
                    fontFamily = FontFamily.Serif,
                    fontSize = 15.sp,
                    textDecoration = TextDecoration.Underline,
                    fontWeight = FontWeight.ExtraBold,
                    textAlign = TextAlign.Start,
                )

            }
        }


        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp, start = 10.dp, end = 10.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_web),
                contentDescription = "ic_website",
                modifier = Modifier.padding(end = 2.dp)
            )

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable(onClick = {
                        school?.website?.let { goToWebsite(it, context) }

                    })
            ) {
                Text(
                    text = school?.website ?: "Website not available",
                    color = Color.Blue,
                    fontFamily = FontFamily.Serif,
                    fontSize = 15.sp,
                    textDecoration = TextDecoration.Underline,
                    fontWeight = FontWeight.ExtraBold,
                    textAlign = TextAlign.Start,
                )

            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 30.dp, start = 10.dp, end = 10.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_pohne),
                contentDescription = "ic_phone",
                modifier = Modifier.padding(end = 2.dp)
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable(onClick = {
                        school?.phone?.let { goToCall(it, context) }

                    })
            ) {
                Text(
                    text = school?.phone ?: "Phone not available",
                    color = Color.Blue,
                    fontFamily = FontFamily.Serif,
                    fontSize = 15.sp,
                    textDecoration = TextDecoration.Underline,
                    fontWeight = FontWeight.ExtraBold,
                    textAlign = TextAlign.Start,
                )
            }
        }

        Text(
            text = "SAT Scores",
            color = Color.DarkGray,
            fontSize = 20.sp,
            fontWeight = FontWeight.ExtraBold,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(top = 30.dp, start = 10.dp, end = 10.dp)
                .fillMaxWidth()
        )

        MyRow(
            title = "Math Avg Score",
            score = sat?.sat_math_avg_score ?: "Score not available"
        )
        MyRow(
            title = "Writing Avg Score",
            score = sat?.sat_writing_avg_score ?: "Score not available"
        )
        MyRow(
            title = "Reading Avg Score",
            score = sat?.sat_critical_reading_avg_score ?: "Score not available"
        )
        MyRow(
            title = "Sat Test Takers",
            score = sat?.num_of_sat_test_takers ?: "Num not available"
        )


    }


}

@Composable
fun DetailsToolbar(navController: NavController) {
    TopAppBar(
        title = { Text(text = "School Details") },
        backgroundColor = Purple700,
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(Icons.Default.ArrowBack, contentDescription = "Back")
            }
        },
    )
}

@Composable
fun MyRow(title: String, score: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp, start = 10.dp, end = 10.dp)
    ) {
        Text(text = title, textAlign = TextAlign.Start,color = Color.Black,
            modifier = Modifier.weight(1f))
        Text(text = score, textAlign = TextAlign.End, modifier = Modifier.weight(1f))

    }

    //Black Line
    Divider(
        modifier = Modifier
            .background(Color.Black)
            .height(1.dp)
            .fillMaxWidth()
            .padding(start = 10.dp, end = 10.dp)
    )

}


// Go to call when you click the Text Phone
fun goToCall(phone: String, context: Context) {
    val dialIntent = Intent(Intent.ACTION_DIAL)
    dialIntent.data = Uri.parse("tel:$phone")
    context.startActivity(dialIntent)
}

// Go to email when you click the Text Email
fun goToEmail(email: String, context: Context) {
    val intent = Intent(Intent.ACTION_SENDTO).apply {
        data = Uri.parse("mailto:$email")
    }
    context.startActivity(intent)

}

// Go to website when you click the Text Website
fun goToWebsite(website: String, context: Context) {
    val uri = Uri.parse("http://$website")
    val intent = Intent(Intent.ACTION_VIEW, uri)
    context.startActivity(intent)
}

// Go to Google maps whe u click on teh Text Address
fun goToGoogleMaps(lat: String?, lon: String?, labelLocation: String, context: Context) {
    val urlAddress =
        "http://maps.google.com/maps?q=$lat,$lon($labelLocation)&iwloc=A&hl=es"
    val gmmIntentUri = Uri.parse(urlAddress)
    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
    mapIntent.setPackage("com.google.android.apps.maps")
    context.startActivity(mapIntent)
}
