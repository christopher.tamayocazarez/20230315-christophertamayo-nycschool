package com.tamayo.nycschoolsjpmc.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.tamayo.nycschoolsjpmc.model.domain.SchoolDomain
import com.tamayo.nycschoolsjpmc.ui.theme.Purple700
import com.tamayo.nycschoolsjpmc.utils.UIState
import com.tamayo.nycschoolsjpmc.viewmodel.SchoolViewModel


@Composable
fun SchoolScreen(schoolViewModel: SchoolViewModel, navController: NavController) {
    when (val state = schoolViewModel.school.observeAsState(UIState.LOADING).value) {
        is UIState.LOADING -> {}
        is UIState.SUCCESS -> {
            SchoolList(schools = state.response, navController) {
                schoolViewModel.selectedSchool = it
                schoolViewModel.dbn = it.dbm.toString()

            }
        }
        is UIState.ERROR -> {}
    }
}

@Composable
fun SchoolList(
    schools: List<SchoolDomain>,
    navController: NavController,
    selectedSchool: ((SchoolDomain) -> Unit)? = null

) {
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        topBar = {
            HomeToolbar()
        }, scaffoldState = scaffoldState, content = {
            it.calculateBottomPadding()
            it.calculateTopPadding()

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = Color.White)
            ) {

                LazyColumn(content = {
                    itemsIndexed(items = schools) { _, school ->
                        SchoolItem(school = school, navController, selectedSchool)
                    }
                })

            }

        })

}


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SchoolItem(
    school: SchoolDomain,
    navController: NavController? = null,
    selectedSchool: ((SchoolDomain) -> Unit)? = null
) {

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(120.dp)
            .padding(horizontal = 20.dp, vertical = 7.dp),
        elevation = 12.dp,
        backgroundColor = Color.White,
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
        onClick = {
            //Get information from school selected
            selectedSchool?.invoke(school)


            //Navigate to details screen
            navController?.navigate("details")
        }
    ) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp), verticalArrangement = Arrangement.Center
        ) {

            Text(
                text = school.name ?: "School name not available",
                fontSize = 16.sp,
                fontWeight = FontWeight.ExtraBold,
                fontFamily = FontFamily.Serif,
                color = Color.Black,
                textAlign = TextAlign.Start,
                modifier = Modifier.padding(5.dp)

            )

            Text(
                text = school.location?.substringBefore("(") ?: "Location not available",
                fontSize = 12.sp,
                fontWeight = FontWeight.Normal,
                color = Color.Black,
                textAlign = TextAlign.Start,
                modifier = Modifier.padding(start = 5.dp, end = 5.dp)

            )

            Text(
                text = school.phone ?: "Phone not available",
                fontSize = 12.sp,
                fontWeight = FontWeight.Normal,
                color = Color.Blue,
                textAlign = TextAlign.Start,
                modifier = Modifier.padding(start = 5.dp, end = 5.dp)

            )

        }

    }

}

@Composable
fun HomeToolbar() {
    TopAppBar(
        title = { Text(text = "NYCSchools") },
        backgroundColor = Purple700,
    )
}



@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {

    SchoolItem(
        SchoolDomain(
            name = "Lazaro cardenas",
            location = "Agustin melgar 140",
            phone = "555 444 5556"
        )
    )

}