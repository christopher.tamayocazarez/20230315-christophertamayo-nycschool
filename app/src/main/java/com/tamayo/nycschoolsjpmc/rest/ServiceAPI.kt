package com.tamayo.nycschoolsjpmc.rest

import com.tamayo.nycschoolsjpmc.model.SATScores
import com.tamayo.nycschoolsjpmc.model.Schools
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceAPI {

    /*
     GET SCHOOLS INFO FROM NYCSchool API
     Schools -> https://data.cityofnewyork.us/resource/s3k6-pzi2.json
     */
    @GET(SCHOOL_PATH)
    suspend fun getSchools(): Response<List<Schools>>

    /*
      GET SAT SCORE INFO FROM NYCSchool API
      Sat Scores -> https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=01M292

     */
    @GET(SAT_PATH)
    suspend fun getSatScores(
        @Query("dbn") dbn: String?
    ): Response<List<SATScores>>


    companion object {
        /*
        Compile time values
         */
        const val BASE_PATH = "https://data.cityofnewyork.us/resource/"
        private const val SCHOOL_PATH = "s3k6-pzi2.json"
        private const val SAT_PATH = "f9bf-2cp4.json"

    }
}


