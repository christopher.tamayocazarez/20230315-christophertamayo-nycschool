package com.tamayo.nycschoolsjpmc.rest

import android.util.Log
import android.widget.Toast
import androidx.compose.ui.platform.LocalContext
import com.tamayo.nycschoolsjpmc.model.domain.SatDomain
import com.tamayo.nycschoolsjpmc.model.domain.SchoolDomain
import com.tamayo.nycschoolsjpmc.model.domain.toDomain
import com.tamayo.nycschoolsjpmc.utils.FailureResponse
import com.tamayo.nycschoolsjpmc.utils.NotConnection
import com.tamayo.nycschoolsjpmc.utils.NullResponse
import com.tamayo.nycschoolsjpmc.utils.UIState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

//private const val TAG = "SchoolRepository"

interface SchoolRepository {


    suspend fun getSchools(): Flow<UIState<List<SchoolDomain>>>
    suspend fun getSatScoresByDbm(dbn: String): Flow<UIState<List<SatDomain>>>


}

class SchoolRepositoryImpl @Inject constructor(
    private val serviceAPI: ServiceAPI,
    private val ioDispatcher: CoroutineDispatcher
) : SchoolRepository {


    /**
     * This method gets all the Schools, and map in my SchoolDomain
     */
    override suspend fun getSchools(): Flow<UIState<List<SchoolDomain>>> = flow {
        emit(UIState.LOADING)

        try {

            val response = serviceAPI.getSchools()
            if (response.isSuccessful) {
                response.body()?.let {
//                    Log.d(TAG, "Response -> $response")
//                    Log.d(TAG, "Response Body -> ${it}")

                    emit(UIState.SUCCESS(it.toDomain()))

                } ?: throw NullResponse()


            } else {
                throw FailureResponse(response.errorBody().toString())
            }

        } catch (e: Exception) {
            println(e.message)
            emit(UIState.ERROR(e))
        }


    }.flowOn(ioDispatcher)


    /**
     * This method gets a selected School, and map in my SatDomain
     */
    override suspend fun getSatScoresByDbm(dbn: String): Flow<UIState<List<SatDomain>>> = flow {
        emit(UIState.LOADING)

        try {

            val response = serviceAPI.getSatScores(dbn)
            if (response.isSuccessful) {
                response.body()?.let {
//                        Log.d(TAG, "Response -> $response")
//                        Log.d(TAG, "Response Body -> ${it}")

                    emit(UIState.SUCCESS(it.toDomain()))

                } ?: throw NullResponse()
            } else throw NotConnection()


        } catch (e: Exception) {
            emit(UIState.ERROR(e))
        }


    }.flowOn(ioDispatcher)

}