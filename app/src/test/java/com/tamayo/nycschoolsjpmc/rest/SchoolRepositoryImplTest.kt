package com.tamayo.nycschoolsjpmc.rest

import com.tamayo.nycschoolsjpmc.model.SATScores
import com.tamayo.nycschoolsjpmc.model.Schools
import com.tamayo.nycschoolsjpmc.model.domain.SatDomain
import com.tamayo.nycschoolsjpmc.model.domain.SchoolDomain
import com.tamayo.nycschoolsjpmc.utils.UIState
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/*
 I am using mockK to mock private objects and use with coroutines
 */

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolRepositoryImplTest {

    private lateinit var testObject: SchoolRepository
    private val mockServiceApi = mockk<ServiceAPI>(relaxed = true)
    private val testDispatcher = UnconfinedTestDispatcher()
    private val testScope = TestScope(testDispatcher)

    private val mockSchoolItem = mockk<Schools>(relaxed = true)
    private val mockSatItem = mockk<SATScores>(relaxed = true)

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {

        Dispatchers.setMain(testDispatcher)
        testObject = SchoolRepositoryImpl(mockServiceApi, testDispatcher)

    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        clearAllMocks()
    }


    @Test
    fun `Get all the schools when server has a list of items and return a SUCCES state`() {
        //AAA


        //Given
        coEvery {
            mockServiceApi.getSchools()
            //When
        } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns
                    listOf(
                        mockSchoolItem
                    )
        }


        val state = mutableListOf<UIState<List<SchoolDomain>>>()
        val job = testScope.launch {
            testObject.getSchools().collect {
                state.add(it)

            }
        }

        //Then
        assertEquals(2, state.size)
        val success = (state[1] as UIState.SUCCESS).response
        assertEquals(1, success.size)

        coVerify { mockServiceApi.getSchools() }
        job.cancel()

    }

    @Test
    fun `Get Sat by DBN info when server return a list of SAT Scores and return a SUCCES state`() {
        //AAA

        //Given
        coEvery {
            mockServiceApi.getSatScores("1234")
            //When
        } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns
                    listOf(
                        mockSatItem
                    )
        }


        val state = mutableListOf<UIState<List<SatDomain>>>()
        val job = testScope.launch {
            testObject.getSatScoresByDbm("1234").collect {
                state.add(it)

            }
        }

        //Then
        assertEquals(2, state.size)
        val success = (state[1] as UIState.SUCCESS).response
        assertEquals(1, success.size)

        coVerify { mockServiceApi.getSatScores("1234") }
        job.cancel()

    }
}